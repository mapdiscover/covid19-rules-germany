var map = L.map('map').setView([51.3, 10.5], 7);
	
map.locate({setView: true, maxZoom: 10});
map.options.maxZoom = 12;

function onLocationFound(e) {
    var radius = e.accuracy;

    L.marker(e.latlng).addTo(map)
        .bindPopup("Ihre Position befindet sich in einem Radius von " + radius + " Metern.").openPopup();

    L.circle(e.latlng, radius).addTo(map);
}

map.on('locationfound', onLocationFound);

L.tileLayer('https://b.tile.openstreetmap.de/{z}/{x}/{y}.png', {
    maxZoom: 12,
    attribution: 'Kartendaten &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap-Mitwirkende</a>, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        '<a href="https://gitlab.com/mapdiscover/covid19-rules-germany">Projektseite</a>, ' +
        '<a href="https://mapdiscover.org/imprint.html">Impressum</a>',
    id: 'mapbox/light-v9',
    tileSize: 512,
    zoomOffset: -1
}).addTo(map);

function onEachFeature(feature, layer) {
    var popupContent = "<p><strong>Informationen über COVID-19 Regelungen</strong> <br> Alle Angaben ohne Gewähr.</p>";

    if (feature.properties) {
        if (feature.properties.name) {
            popupContent += "<strong>Name/Kreis:</strong> " + feature.properties.name;
        }

        popupContent += getInformation("Maskenpflicht", feature.properties.mandatoryMask);
        popupContent += getInformation("Versammlung", feature.properties.personLimit);
        popupContent += getInformation("Sperrstunde", feature.properties.closingTime);

        if (feature.properties.other && feature.properties.other != null && feature.properties.other != "") {
            popupContent += "<p><strong>Weiteres:</strong> " + feature.properties.other;
        }

        popupContent += `<p><small><a href="https://gitlab.com/mapdiscover/covid19-rules-germany">Die Daten stimmen nicht? Du willst etwas ergänzen?</a></small>`;
    }

    layer.bindPopup(popupContent);
}

function getInformation(infoName, info) {
    var prefixBlock = "<p><strong>" + infoName + ":</strong> ";

    if (info == undefined || info == null || info.info == undefined || info.info == null) {
        return prefixBlock + "Keine Daten.";
    }

    var sourceBlock = "Keine Quelle angegeben";

    if (info.source != undefined && info.source != null) {
        if (info.lastUpdate != undefined && info.lastUpdate != null) {
            sourceBlock = "Stand: " + info.lastUpdate + ", ";
        }
        else
        {
            sourceBlock = "Kein Abrufdatum angegeben, ";
        }

        sourceBlock += `<a href="` + info.source + `">Quelle</a>`;
    }

    return prefixBlock + info.info + " <br>(" + sourceBlock + ")";
}

var myStyle = {
    weight: 2,
    color: "#999",
    opacity: 0.5,
    fillColor: "#B0DE5C",
    fillOpacity: 0.5
}

L.geoJSON(vwg, {
    style: myStyle,
    onEachFeature: onEachFeature
}).addTo(map);
